import os

import requests
from dotenv import load_dotenv

from coins.models import FavorCoinModel

load_dotenv('.env')

NewsApiKey = os.getenv('NewsApiKey')





def get_news_list():
    url = f'https://newsapi.org/v2/everything?apiKey={NewsApiKey}&language=ru&pageSize=5&q=криптовалюта'
    response = requests.get(url).json()
    return response['articles']


def get_coins_list():
    new_response = []
    headers = {"X-CMC_PRO_API_KEY": os.getenv('CoinsAPIKey')}
    base_url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/'
    url = 'listings/latest?limit=100'
    response = requests.get(f"{base_url}{url}", headers=headers).json()
    for el in response['data']:
        new_response.append({
            'id': el['id'],
            'name': el['name'],
            'symbol': el['symbol'],
            'price': el['quote']['USD']['price'],
            'volume_24h': el['quote']['USD']['volume_24h'],
            'volume_change_24h': el['quote']['USD']['volume_change_24h'],
            'percent_change_24h': el['quote']['USD']['percent_change_24h'],
        })

    return new_response


def get_base_context(self):

    if self.request.user.is_authenticated:
        favor_coin = FavorCoinModel.objects.filter(user=self.request.user)
        news_list = get_news_list()
        favor_coin_id = []
        for c in favor_coin:
            favor_coin_id.append(c.coin.id)

        context = {
            'favor_coin': favor_coin,
            'favor_coin_id': favor_coin_id,
            'news_list': news_list,
            'user': self.request.user,
            'is_authenticated': self.request.user.is_authenticated,
        }
        return context
    return {'is_authenticated': False}
