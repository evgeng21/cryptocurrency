from django.contrib import admin

from coins.models import CoinModel, FavorCoinModel

# Register your models here.
admin.site.register(CoinModel)
admin.site.register(FavorCoinModel)
