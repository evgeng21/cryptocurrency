from django.db.models import Q
from rest_framework import filters
from rest_framework.generics import CreateAPIView
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from coins.models import CoinModel, FavorCoinModel
from coins.serializers import CoinSerializer, FillCoinsSerializer, FavorCoinSerializer, RegisterUserSerializer
from coins.utils import get_coins_list, get_base_context


# Получение списка валют и детальной информации по символу
class CoinViewSet(ModelViewSet):
    queryset = CoinModel.objects.all()
    serializer_class = CoinSerializer
    lookup_field = 'symbol'
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'symbol']


# Заполнить данными из внешнего API
class FillCoins(CreateAPIView):

    def post(self, request, *args, **kwargs):
        coins_data = get_coins_list()
        for coin in coins_data:
            serializer = FillCoinsSerializer(data=coin)
            serializer.is_valid(raise_exception=True)
            serializer.save()
        return Response({'message': 'Well done!'})


# Получить список избранных, добавить, удалить API
class FavorCoinViewSet(ModelViewSet):
    queryset = FavorCoinModel.objects.all()
    serializer_class = FavorCoinSerializer


# Список избранных для шаблона с фильтрацией по текущеиу пользователю
class CoinListTemplate(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'coins/index.html'

    def get(self, request, *args, **kwargs):
        search = request.GET.get('search', None)
        if search:
            queryset = CoinModel.objects.filter(Q(name__icontains=search) | Q(symbol__icontains=search))
        else:
            queryset = CoinModel.objects.all()
        context = {'coin_list': CoinSerializer(queryset, many=True).data}
        context.update(get_base_context(self))
        return Response({'data': context})


# Добавить в избранное из шаблона
class CreateFavorCoin(CreateAPIView):
    serializer_class = FavorCoinSerializer

    def post(self, request, *args, **kwargs):
        favor_coin = FavorCoinModel.objects.filter(Q(user=self.request.user) & Q(coin=self.request.POST.get('coin')))
        if favor_coin:
            favor_coin.delete()
            return Response({'message': 'delete'})

        serializer = FavorCoinSerializer(data={
            'coin': self.request.POST.get('coin'),
            'user': self.request.user.id
        })
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({'message': 'OK'})


# Регистрация пользователя
class RegisterUser(CreateAPIView):
    serializer_class = RegisterUserSerializer

    def post(self, request, *args, **kwargs):
        print(self.request.POST)
        serializer = RegisterUserSerializer(data=self.request.POST)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
