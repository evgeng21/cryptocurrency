from django.db import models
from rest_framework.authtoken.admin import User


class CoinModel(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    name = models.CharField(max_length=50)
    symbol = models.CharField(max_length=50, unique=True)
    price = models.FloatField()
    volume_24h = models.FloatField()
    volume_change_24h = models.FloatField()
    percent_change_24h = models.FloatField()

    def __str__(self):
        return ''.join(f'{self.name}({self.symbol})')


class FavorCoinModel(models.Model):
    coin = models.ForeignKey(CoinModel, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return ''.join(f'{self.coin.name}({self.user})')

    class Meta:
        unique_together = ('user', 'coin')
