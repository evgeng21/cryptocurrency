from rest_framework import serializers, filters
from rest_framework.authtoken.admin import User

from coins.models import CoinModel, FavorCoinModel


class CoinSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoinModel
        fields = '__all__'
        lookup_field = 'symbol'
        filter_backends = [filters.SearchFilter]
        search_fields = ['name', 'symbol']


class FillCoinsSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=50)
    symbol = serializers.CharField(max_length=50)
    price = serializers.FloatField()
    volume_24h = serializers.FloatField()
    volume_change_24h = serializers.FloatField()
    percent_change_24h = serializers.FloatField()

    def create(self, validated_data):
        instance = CoinModel.objects.update_or_create(
            id=validated_data['id'],
            defaults={
                'id': validated_data['id'],
                'name': validated_data['name'],
                'symbol': validated_data['symbol'],
                'price': validated_data['price'],
                'volume_24h': validated_data['volume_24h'],
                'volume_change_24h': validated_data['volume_change_24h'],
                'percent_change_24h': validated_data['percent_change_24h'],
            }
        )
        return instance


class FavorCoinSerializer(serializers.ModelSerializer):

    class Meta:
        model = FavorCoinModel
        fields = '__all__'


class RegisterUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
