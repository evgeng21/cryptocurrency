from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from coins.views import CoinViewSet, FillCoins, CoinListTemplate, FavorCoinViewSet, CreateFavorCoin, RegisterUser

router = DefaultRouter()

router.register(r'coins', CoinViewSet, basename='coins')
router.register(r'favor_coins', FavorCoinViewSet, basename='favor_coins')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(router.urls)),
    path('api/v1/fill_coins', FillCoins.as_view(), name='fill_coins'),
    path('api/v1/drf-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/v1/register/', RegisterUser.as_view(), name='register'),
    path('', CoinListTemplate.as_view(), name='home'),
    path('add_favor/', CreateFavorCoin.as_view(), name='add_favor'),
]